﻿using UnityEngine;

using System.IO;
using System.Data;
public class RestartSub : MonoBehaviour
{
    GameObject turret;
    public void OnRestartToggled(object sender, RestartEvent args)
    {
        string directoryPath = "C:/POO";
        string filePath = "C:/POO/" + System.DateTime.Now.Year.ToString() + System.DateTime.Now.Day.ToString() + System.DateTime.Now.Month.ToString() + System.DateTime.Now.Hour.ToString() + System.DateTime.Now.Minute.ToString() + System.DateTime.Now.Second.ToString() + ".txt";
        int x = UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex;
        if (!Directory.Exists(directoryPath))
            Directory.CreateDirectory(directoryPath);
        if (!File.Exists(filePath))
        {
            using (StreamWriter sw = File.CreateText(filePath))
            {
                sw.WriteLine("The level restarted with the enumerable " + args.actualEnum);
                sw.WriteLine("The player switched colors a total of: " + args.Player.GetComponent<PlayerController>().cSwitchTimes + " times");
                sw.WriteLine("The last color used was: " + args.Player.GetComponent<PlayerController>().lastColor);
                sw.WriteLine("By the time of this text, the player had died a total of: " + args.Player.GetComponent<PlayerController>().deaths + " times");
                sw.WriteLine("Last level played: " + MasterScript.levelInfo[x].sceneName + " of level number: " + x);
                sw.WriteLine("The bullets were shot a total of: " + args.HitsBullet.Count + " times");
                sw.WriteLine("Type of bullets shot:");
                foreach (var bullets in args.HitsBullet)
                {
                    sw.Write(bullets.TypeOfBullet + " ");
                }
                sw.Close();
            }
        }
        Controller_PlayerColor.amount = -1;//Debug and seter of amount//
        args.Player.GetComponent<PlayerController>().AskToWarp(); //Reset Color of player (swap)//
        turret = GameObject.Find("Enemy"); //Turret GameObject Find//
        turret.GetComponent<Torreta>().cont = 1; //Switch Timer to one//
        if (args.ball != null)
            Destroy(args.ball.gameObject); //Destroy all bullets//
        args.TowerGameObject.historicalBullets.Clear(); //Clear generic list//
        Time.timeScale = 1;  //Return TimeScale to normality//
    }
}
