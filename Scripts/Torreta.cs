﻿using System.Collections.Generic;
using UnityEngine;
using System;
using System.Collections;

public class Torreta : MonoBehaviour, IEnumerable<Controller_Bullet>
{
    public List<GameObject> bullets = new List<GameObject>();
    public List<Controller_Bullet> historicalBullets = new List<Controller_Bullet>();

    #region MonoBehaviour
    public Transform disparador;
    public Transform player;
    public float maxCount;
    public float cont;
    public static Rigidbody balaPrefab;
    public static GameObject selfTorreta;
    GameObject balaSaviour;
    GameObject bala;
    private int bulletInstantiated;

    void Start()
    {
        cont = maxCount;
        balaSaviour = GameObject.Find("BulletSaviour");
        selfTorreta = this.gameObject;
    }
    void Update()
    {
        cont -= Time.deltaTime;
        disparador.transform.LookAt(player);
        if (cont < 0)
        {
            Instantiator();
        }
    }
    void Instantiator()
    {
        balaPrefab = Instantiate(bullets[bulletInstantiated = UnityEngine.Random.Range(0, 3)].GetComponent<Rigidbody>(), disparador.position, Quaternion.identity);
        balaPrefab.name = "Bullet(Clone)";
        if (bala == null)
        {
            bala = GameObject.Find("Bullet(Clone)");
            bala.transform.SetParent(balaSaviour.transform);
        }
        else
        {
            balaPrefab.transform.SetParent(bala.transform);
        }
        cont = 1.5f;
        TurretHistoricalAdditive.instantiateBulletHistorical(bulletInstantiated);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Destroy(bala); // Agregación.
            Destroy(gameObject);
        }
    }
    #endregion
    public void AddInTheHistory(Controller_Bullet bulletHist)
    {
        historicalBullets.Add(bulletHist);
    }
    public IEnumerator<Controller_Bullet> GetEnumerator()
    {
        return historicalBullets.GetEnumerator();
    }
    IEnumerator IEnumerable.GetEnumerator()
    {
        return historicalBullets.GetEnumerator();
    }
}


public class TurretHistoricalAdditive
{
    public static void instantiateBulletHistorical(int bulletHistorical)
    {
        GameObject newBullet = Torreta.balaPrefab.gameObject;
        switch (bulletHistorical)
        {
            case 0:
                newBullet.GetComponent<Controller_Bullet>().TypeOfBullet = "NormalBullet";
                Torreta.selfTorreta.GetComponent<Torreta>().AddInTheHistory(newBullet.GetComponent<Controller_Bullet>());
                break;
            case 1:
                newBullet.GetComponent<Controller_Bullet>().TypeOfBullet = "FastBullet";
                Torreta.selfTorreta.GetComponent<Torreta>().AddInTheHistory(newBullet.GetComponent<Controller_Bullet>());
                break;
            case 2:
                newBullet.GetComponent<Controller_Bullet>().TypeOfBullet = "CrashingBullet";
                Torreta.selfTorreta.GetComponent<Torreta>().AddInTheHistory(newBullet.GetComponent<Controller_Bullet>());
                break;
            default:
                break;
        }
    }
}
