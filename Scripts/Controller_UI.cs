﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_UI : MonoBehaviour
{
    public MasterScript mScript;

    public void OnResumePressed()
    {
        mScript.panelButtons.SetActive(false);
        Time.timeScale = 1;
    }
    public void OnDefaultInfoPressed()
    {
        mScript.ImageWithInfo.SetActive(true);
        mScript.ThrowDefaultInfo();
    }
    public void OnDebugInfoPressed()
    {
        mScript.ImageWithInfo.SetActive(true);
        mScript.ThrowDebugInfo();
    }
    public void OnBackPressed()
    {
        mScript.ImageWithInfo.SetActive(false);
    }
}
