﻿using UnityEngine;

public abstract class Controller_Bullet : MonoBehaviour
{
    //Clase abstracta, sólo la puedo heredar pero jamás instanciar, no requiere heredar de MonoBehaviour para éste script, pero por
    //comodidad lo hice para las clases que se heredan
    public string TypeOfBullet { get; set; }
    internal Rigidbody rb;
    internal GameObject disparador;
    internal float velDisparo;
}
