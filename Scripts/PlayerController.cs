﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class PlayerController : MonoBehaviour, IJugador
{
    private Rigidbody rb;
    private bool enElSuelo = true;
    public float Impulso;

    public Controller_PlayerColor pColor;
    public MasterScript master;
    public GameObject turret;
    public static Vector3 startPos;

    //IMPLEMENTATION OF INTERFACE
    public float velocity { get; set; }
    public int cSwitchTimes { get; set; }
    public string lastColor { get; set; }
    public bool canChangeColor { get; set; }
    public int deaths { get; set; }
    public void resolveChangeColor(bool yesOrNo)
    {
        if (yesOrNo == canChangeColor)
        {
            pColor.ModifyValue();
            lastColor = pColor.color.ToString();
            cSwitchTimes++;
            canChangeColor = false;
            MasterScript.sCPlayerValue += cSwitchTimes;
        }
    }

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
        GameObject masterScript = GameObject.Find("MasterScript");
        turret = GameObject.Find("Enemy");
        master = masterScript.GetComponent<MasterScript>();
        pColor = masterScript.GetComponent<Controller_PlayerColor>();
        master.OnChangedScene();
        startPos = this.transform.position;
    }

    public void AskToWarp()
    {
        if (Time.timeScale == 1)
        {
            transform.position = startPos;
            rb.velocity = Vector3.zero;
            pColor.ModifyValue();
        }
    }

    void Update()
    {

        float movimientoAdelanteAtras = Input.GetAxis("Vertical") * velocity;
        float movimientoCostados = Input.GetAxis("Horizontal") * velocity;

        movimientoAdelanteAtras *= Time.deltaTime;
        movimientoCostados *= Time.deltaTime;

        transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);

        if (Input.GetKeyDown(KeyCode.R))
        {
            if (turret != null)
                EjectReset(); 
                
        }
    }
    private void EjectReset()
    {
        master.OnReset(pColor.color.GetHashCode(), GameObject.Find("Bullet(Clone)"), this.gameObject, turret.GetComponent<Torreta>(), turret.GetComponent<Torreta>().historicalBullets);
        cSwitchTimes = 0;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (!collision.gameObject.CompareTag("Bullet") || !collision.gameObject.CompareTag("Torreta"))
        {
            if (collision.gameObject.CompareTag("Floor") || (collision.gameObject.CompareTag("Save") && pColor.color != 0) || (collision.gameObject.CompareTag("Blue") && pColor.color != (Controller_PlayerColor.ColorPlayer)1) || (collision.gameObject.CompareTag("Purple") && pColor.color != (Controller_PlayerColor.ColorPlayer)2) || (collision.gameObject.CompareTag("Red") && pColor.color != (Controller_PlayerColor.ColorPlayer)3))
            {
                EjectReset();
                deaths++;
            }
            else
            {
                enElSuelo = true;
            }
        }
        if (collision.gameObject.CompareTag("Bullet"))
        {
            canChangeColor = true;
            resolveChangeColor(true);
            if (enElSuelo == true)
            {
                rb.velocity = new Vector3(0f, Impulso, 0f);
                enElSuelo = false;
            }
        }
        if (collision.gameObject.CompareTag("Torreta"))
        {
            Time.timeScale = 0.8f;
            master.sceneChanged = true;
        }
    }
}
