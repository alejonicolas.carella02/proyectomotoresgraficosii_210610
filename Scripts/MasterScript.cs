﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MasterScript : MonoBehaviour
{
    public static List<Controller_LevelExchanger> levelInfo = new List<Controller_LevelExchanger>();
    GameObject player;
    public static int sCPlayerValue = 0;
    public GameObject canvas;
    public GameObject panelButtons;
    public GameObject ImageWithInfo;
    public Text infoText;
    public bool sceneChanged;
    int actualLevel = 1;
    private void Awake()
    {
        #region USE OF UI INTERFACE
        DontDestroyOnLoad(canvas.gameObject);
        panelButtons.SetActive(false);
        ImageWithInfo.SetActive(false);
        #endregion

        #region USE OF LEVELINFO
        DontDestroyOnLoad(this.gameObject);
        int sceneLength = SceneManager.sceneCountInBuildSettings;
        Controller_LevelExchanger newObject;
        for (int i = 0; i < sceneLength; i++)
        {
            string a = System.IO.Path.GetFileNameWithoutExtension(SceneUtility.GetScenePathByBuildIndex(i));
            if (a == "MainMenu")
            {
                newObject = new Controller_LevelExchanger(i) { sceneName = a };
            }
            else
            {
                newObject = new Controller_LevelExchanger(a)
                { actualLevel = i };
            }
            levelInfo.Add(newObject);
        }
        ResearchLevelInfo();
        #endregion
    }
    public void ChangeScene()
    {
        if (actualLevel < 4)
        {
            actualLevel++;
            SceneManager.LoadScene(levelInfo[actualLevel].actualLevel, LoadSceneMode.Single);
        }
        else
        {
            SceneManager.LoadScene(0, LoadSceneMode.Single);
            levelInfo.Clear();
            Destroy(this.gameObject);
            Destroy(canvas.gameObject);
        }

    }
    public void OnChangedScene()
    {
        #region USE OF PLAYER INTERFACE
        if (player != null)
            player = null;
        player = GameObject.Find("Player");
        player.GetComponent<PlayerController>().velocity = 6;
        player.GetComponent<PlayerController>().cSwitchTimes = 0;
        player.GetComponent<PlayerController>().lastColor = player.GetComponent<PlayerController>().pColor.color.ToString();
        player.GetComponent<PlayerController>().canChangeColor = true;
        GetComponent<Controller_PlayerColor>().player = player;
        Controller_PlayerColor.amount = 0;
        GetComponent<Controller_PlayerColor>().NewScene();
        Time.timeScale = 1;
        #endregion
    }
    private void FixedUpdate()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            panelButtons.SetActive(true);
            Time.timeScale = 0;
        }
        if (sceneChanged)
        {
            ChangeScene();
            sceneChanged = false;
        }
    }
    public void ThrowDefaultInfo()
    {
        levelInfo.Sort();
        int x = SceneManager.GetActiveScene().buildIndex;
        infoText.text = "-Actual Level: " + x  + "\n-Level name: " + levelInfo[x].sceneName + "\n-You've died " + player.GetComponent<PlayerController>().deaths + " times";
    }
    public void ThrowDebugInfo()
    {
        string value;
        GameObject turret = GameObject.Find("Enemy");
        if (player.GetComponent<PlayerController>().canChangeColor)
            value = "yes";
        else
            value = "no";
        infoText.text = "-Player velocity: " + player.GetComponent<PlayerController>().velocity + "\n-Times the color of the player on the actual run: " + player.GetComponent<PlayerController>().cSwitchTimes + "\n-Times the color of the player on the history: " + sCPlayerValue +  "\n-Player can switch color?: " + value + "\n-Bullets shot: " + turret.GetComponent<Torreta>().historicalBullets.Count;
    }
    
    private void ResearchLevelInfo()
    {
        levelInfo.Sort(new LevelComparer.StringLevelComparer());
        foreach (Controller_LevelExchanger info in levelInfo)
            Debug.Log("Niveles ordenados por su nombre: " + info.sceneName + " Para el nivel: " + info.actualLevel);
        levelInfo.Sort();
        Debug.Log("----------------");
        foreach (Controller_LevelExchanger info in levelInfo)
            Debug.Log("Niveles ordenados por su nivel: " + info.sceneName + " Para el nivel: " + info.actualLevel);
    }


    public void OnReset(int datoEnum, GameObject bullet, GameObject player, Torreta towerEnemy, List<Controller_Bullet> histBullet)
    {
        var ResetPub = new Restart();
        var ResetSub = new RestartSub();
        ResetPub.eventoReset += ResetSub.OnRestartToggled;
        ResetPub.OnRestart(datoEnum, bullet, player, towerEnemy, histBullet);
    }
}
