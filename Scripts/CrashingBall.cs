﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrashingBall : Controller_Bullet
{
    int timesCol;
    int actualVelocity;
    private void Awake()
    {
        disparador = GameObject.Find("Instantiator_Bullets");
        rb = GetComponent<Rigidbody>();
        velDisparo = 6;
        rb.AddForce(disparador.transform.forward * 100 * velDisparo);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (!collision.gameObject.CompareTag("Player"))
        {
            if (timesCol != 4)
            {
                Vector3 dir = collision.contacts[0].point - transform.position;
                dir = -dir.normalized;
                rb.AddForce(dir * 6, ForceMode.Impulse);
                timesCol++;
                actualVelocity += 2;
                velDisparo += velDisparo / actualVelocity;
            }
            else
                Destroy(gameObject);
        }
        if (collision.gameObject.CompareTag("Player"))
            Destroy(gameObject);
    }
}
