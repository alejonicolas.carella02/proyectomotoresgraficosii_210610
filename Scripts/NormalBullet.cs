﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NormalBullet : Controller_Bullet
{
    private void Awake()
    {
        disparador = GameObject.Find("Instantiator_Bullets");
        rb = GetComponent<Rigidbody>();
        velDisparo = 10;
        rb.AddForce(disparador.transform.forward * 100 * velDisparo);
    }
    private void OnCollisionEnter(Collision collision)
    {
            Destroy(gameObject);
    }
}
