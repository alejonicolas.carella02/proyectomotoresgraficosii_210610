﻿using System.Collections;
using UnityEngine;

public class FastBullet : Controller_Bullet
{
    private void Awake()
    {
        disparador = GameObject.Find("Instantiator_Bullets");
        rb = GetComponent<Rigidbody>();
        velDisparo = 14;
        rb.AddForce(disparador.transform.forward * 100 * velDisparo);
    }
    private void OnCollisionEnter(Collision collision)
    {
            Destroy(gameObject);
    }
}
