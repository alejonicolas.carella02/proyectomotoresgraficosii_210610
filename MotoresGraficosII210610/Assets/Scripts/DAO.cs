﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Data.SqlClient;
using System.Net.Security;
using System.Data.Sql;
using System.Data;
using System.Globalization;

public class DAO : MonoBehaviour
{
    SqlConnection sCon = new SqlConnection("Data Source =.; Initial Catalog = TrabajoPracticoPA; User ID = hoster; Password = 1234");
    private int ClientID;
    private float bestTime;
    private string tipo;

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
        if (sCon.State == System.Data.ConnectionState.Open)
            sCon.Close();
    }
   
    /* private void Awake()
     {
         SqlDataAdapter mDa = new SqlDataAdapter("SELECT ID_Player FROM Table_Players WHERE username = '" + "hola" + "'", sCon);
         DataSet mDs = new DataSet();
         mDa.Fill(mDs);
         if(mDs.Tables[0].Rows.Count > 0)
         Debug.Log((int)mDs.Tables[0].Rows[0].ItemArray[0]);
     }*/
    public void FirstExecutableSQL(string username, string playerType)
    {
        tipo = playerType;
        sCon.Open();
        SqlDataAdapter mDa = new SqlDataAdapter("SELECT * FROM Table_Players WHERE username = '" + username + "'", sCon);
        DataSet mDs = new DataSet();
        mDa.Fill(mDs);
        if (mDs.Tables[0].Rows.Count > 0)
        ClientID = (int)mDs.Tables[0].Rows[0].ItemArray[0];
        else
        {
            SqlCommand sCom2 = new SqlCommand("SELECT ISNULL(MAX(ID_Player), 0) As ID FROM Table_Players", sCon);
            int parse = int.Parse(sCom2.ExecuteScalar().ToString());
            parse += 1;
            ClientID = parse;
            string command = "INSERT INTO Table_Players (ID_Player, username, player_pos_x, player_pos_y, player_pos_z, bullets_shot, last_player_type, time_died_as_player, games_played, most_time_alive) VALUES (" + ClientID + ",'" + username + "', 0, 0, 0, 0, '" + playerType + "', 0, 1, 0)";
            SqlCommand sCom3 = new SqlCommand(command, sCon);
            sCom3.ExecuteNonQuery();
            if (playerType == "turret")
            {
                SqlCommand posCom = new SqlCommand("UPDATE Table_Players SET player_pos_x = -8, player_pos_y = 2.3, player_pos_z = 28.35 WHERE ID_Player = " + ClientID, sCon);
                posCom.ExecuteNonQuery();
            }
            else
            {
                SqlCommand posCom = new SqlCommand("UPDATE Table_Players SET player_pos_x = -8, player_pos_y = 2, player_pos_z = 2.4 WHERE ID_Player = " + ClientID, sCon);
                posCom.ExecuteNonQuery();
            }
            sCon.Close();
            bestTime = 0;
            return;
        }
        SqlCommand sCom = new SqlCommand("UPDATE Table_Players SET last_player_type = '" + playerType + "', games_played = games_played + " + 1 + " WHERE ID_Player = " + ClientID, sCon);
        sCom.ExecuteNonQuery();
        if (playerType == "player")
            bestTime = float.Parse(mDs.Tables[0].Rows[0].ItemArray[9].ToString());
        sCon.Close();
    }

    public void valuePosition(GameManager master)
    {
        sCon.Open();
            SqlDataAdapter mDa = new SqlDataAdapter("SELECT * FROM Table_Players WHERE ID_Player = " + ClientID, sCon);
            DataSet mDs = new DataSet();
            mDa.Fill(mDs);
            Vector3 pos = new Vector3(float.Parse(mDs.Tables[0].Rows[0].ItemArray[2].ToString()), float.Parse(mDs.Tables[0].Rows[0].ItemArray[3].ToString()), float.Parse(mDs.Tables[0].Rows[0].ItemArray[4].ToString()));
            master.positionPlayer = pos;  
        sCon.Close();
    }
    public void OnAddingValuesToSQL(string values, string rowNeed)
    {
        sCon.Open();
        switch (rowNeed)
        {
            case "player_pos_x":
                var d = values.Replace(",", ".");
                SqlCommand sComX = new SqlCommand("UPDATE Table_Players SET player_pos_x = " + d + " WHERE ID_Player = " + ClientID, sCon);
                sComX.ExecuteNonQuery();
                break;
            case "player_pos_y":
                var d2 = values.Replace(",", ".");
                SqlCommand sComY = new SqlCommand("UPDATE Table_Players SET player_pos_y = " + d2 + " WHERE ID_Player = " + ClientID, sCon);
                sComY.ExecuteNonQuery();
                break;
            case "player_pos_z":
                var d3 = values.Replace(",", ".");
                SqlCommand sComZ = new SqlCommand("UPDATE Table_Players SET player_pos_z = " + d3 + " WHERE ID_Player = " + ClientID, sCon);
                sComZ.ExecuteNonQuery();
                break;
            case "bullets_shot":
                SqlCommand sComBull = new SqlCommand("UPDATE Table_Players SET bullets_shot = bullets_shot + " + values + " WHERE ID_Player = " + ClientID, sCon);
                sComBull.ExecuteNonQuery();
                break;
            case "time_died_as_player":
                if (tipo == "player")
                {
                    SqlCommand sComDed = new SqlCommand("UPDATE Table_Players SET time_died_as_player = time_died_as_player + " + values + " WHERE ID_Player = " + ClientID, sCon);
                    sComDed.ExecuteNonQuery();
                }
                break;
            case "games_played":
                SqlCommand sComGam = new SqlCommand("UPDATE Table_Players SET games_played = games_played + " + values + "WHERE ID_Player = " + ClientID, sCon);
                sComGam.ExecuteNonQuery();
                break;
            case "most_time_alive":
               if (bestTime < float.Parse(values) && tipo == "player")
               {
                    SqlCommand sTime = new SqlCommand("UPDATE Table_Players SET most_time_alive = " + values + " WHERE ID_Player =" + ClientID, sCon);
                    sTime.ExecuteNonQuery();
               }
                break;
            default:
                break;
        }
        sCon.Close();
    }










  /*  public void OnRecievingInfoFromPhoton(string username)
    {
        sCon.Open();
        SqlCommand sCom = new SqlCommand("SELECT * FROM Table_Players WHERE username = '" + username + "'");
        var result = sCom.ExecuteNonQuery();
        if (result != null)
        {
            Debug.Log(result);
        }
        else Debug.Log("AAAA");
        sCon.Close();
        //INSERT INTO Jugador_Table(ID_Jugador, username, player_pos_x, player_pos_y, player_pos_z, death_times, level_index, bullets_shot_turret, bullets_recieved_turret, last_player_type) VALUES(0, 'Alejo', 12, 2, 4, 0, 1, 2, 2, 'player')

    }
  */
    /* public void OnRetrievingInformation(object sender, GetWeaponDelegate args)
      {
          sCon.Open();
          SqlCommand sCom = new SqlCommand("SELECT ISNULL(MAX(Arma_ID), 0) As Ultimo_ID FROM Arma", sCon);
          int parse = int.Parse(sCom.ExecuteScalar().ToString());
          parse += 1;
          string command = "INSERT INTO Arma (Arma_ID, Arma_Nombre, Arma_Marca, Arma_Disparos, Arma_Potencia) VALUES (" + parse + ", '" + args.NombreArma + "', '" + args.MarcaArma + "', " + args.DisparosArma + ", " + args.PotenciaArma + ")";
          SqlCommand sCom2 = new SqlCommand(command, sCon);
          sCom2.ExecuteNonQuery();
          sCon.Close();
      }

      public void Eliminar(int dato)
      {
          sCon.Open();
          string command = "DELETE FROM Arma WHERE Arma_ID = " + dato;
          SqlCommand sCom = new SqlCommand(command, sCon);
          sCom.ExecuteNonQuery();
          sCon.Close();
      }
      public List<Arma> SQLActualizar()
      {
          sCon.Open();
          armasCargadasenVisual.Clear();
          string command = "SELECT * FROM Arma";
          SqlDataAdapter mDa = new SqlDataAdapter(command, sCon);
          DataSet mDs = new DataSet();
          mDa.Fill(mDs);

          for (int i = 0; i < mDs.Tables[0].Rows.Count; i++)
          {
              Arma arma = new Arma();
              arma.ID = (int)mDs.Tables[0].Rows[i].ItemArray[0];
              arma.Nombre = (string)mDs.Tables[0].Rows[i].ItemArray[1];
              arma.Marca = (string)mDs.Tables[0].Rows[i].ItemArray[2];
              arma.Disparos = (int)mDs.Tables[0].Rows[i].ItemArray[3];
              arma.Potencia = (int)mDs.Tables[0].Rows[i].ItemArray[4];
              armasCargadasenVisual.Add(arma);
          }
          sCon.Close();
          return armasCargadasenVisual;
      }*/
}
