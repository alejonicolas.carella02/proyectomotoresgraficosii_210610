﻿using System.Collections.Generic;
using UnityEngine;
using System;
using System.Collections;

public class Torreta : Photon.MonoBehaviour, IEnumerable<Controller_Bullet>
{
    public List<GameObject> bullets = new List<GameObject>();
    public List<Controller_Bullet> historicalBullets = new List<Controller_Bullet>();

    public PhotonView photonView;
    public GameObject selfCamera;
    #region MonoBehaviour
    public Transform disparador;
    public float maxCount;
    public float cont;
    public static GameObject balaPrefab;
    public static GameObject selfTorreta;
    GameObject balaSaviour;
    GameObject bala;
    private int bulletInstantiated;

    void Start()
    {
        cont = maxCount;
        balaSaviour = GameObject.Find("BulletSaviour");
        selfTorreta = this.gameObject;
        if (!photonView.isMine) selfCamera.GetComponent<Camera>().enabled = false;
    }
    void Update()
    {
        cont -= Time.deltaTime;

        if (cont < 0 && Input.GetKeyDown(KeyCode.Space))
        {
            balaPrefab = PhotonNetwork.Instantiate(bullets[bulletInstantiated = UnityEngine.Random.Range(0, 3)].name, disparador.transform.position, selfCamera.transform.rotation, 0);
            photonView.RPC("Instantiator", PhotonTargets.All);
            cont = 1.5f;
        }
    }

    [PunRPC]
    void Instantiator()
    {
        //balaPrefab = PhotonNetwork.Instantiate(bullets[bulletInstantiated = UnityEngine.Random.Range(0, 3)].name, disparador.transform.position, selfCamera.transform.rotation, 0);
        balaPrefab.name = "Bullet(Clone)";
        if (bala == null)
        {
            bala = GameObject.Find("Bullet(Clone)");
            bala.transform.SetParent(balaSaviour.transform);
        }
        else
        {
            balaPrefab.transform.SetParent(bala.transform);
        }
        historicalBullets.Add(balaPrefab.GetComponent<Controller_Bullet>());

        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Destroy(bala); // Agregación.
            Destroy(gameObject);
        }
    }
    #endregion
    public void AddInTheHistory(Controller_Bullet bulletHist)
    {
        historicalBullets.Add(bulletHist);
    }
    public IEnumerator<Controller_Bullet> GetEnumerator()
    {
        return historicalBullets.GetEnumerator();
    }
    IEnumerator IEnumerable.GetEnumerator()
    {
        return historicalBullets.GetEnumerator();
    }
}


public class TurretHistoricalAdditive
{
    public static void instantiateBulletHistorical(int bulletHistorical)
    {
        GameObject newBullet = Torreta.balaPrefab.gameObject;
        switch (bulletHistorical)
        {
            case 0:
                newBullet.GetComponent<Controller_Bullet>().TypeOfBullet = "NormalBullet";
                Torreta.selfTorreta.GetComponent<Torreta>().AddInTheHistory(newBullet.GetComponent<Controller_Bullet>());
                break;
            case 1:
                newBullet.GetComponent<Controller_Bullet>().TypeOfBullet = "FastBullet";
                Torreta.selfTorreta.GetComponent<Torreta>().AddInTheHistory(newBullet.GetComponent<Controller_Bullet>());
                break;
            case 2:
                newBullet.GetComponent<Controller_Bullet>().TypeOfBullet = "CrashingBullet";
                Torreta.selfTorreta.GetComponent<Torreta>().AddInTheHistory(newBullet.GetComponent<Controller_Bullet>());
                break;
            default:
                break;
        }
    }
}
