﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameObject prefabPlayer, prefabTurret;
    public GameObject referencePlayer, referenceTurret;
    public GameObject turretScene;
    public Vector3 positionPlayer;
    DAO DAO;
    public void Awake()
    {
      
        DAO = GameObject.Find("DAO").GetComponent<DAO>();
        DAO.valuePosition(this.gameObject.GetComponent<GameManager>());
        if (PhotonNetwork.room.PlayerCount == 1)//instancia jugador (el servidor lo hace)
        {
            PhotonNetwork.Instantiate(prefabPlayer.name, positionPlayer, Quaternion.identity, 0);
            return;
        }
        else//si hay más de un jugador se instancia la torreta
        {
            turretScene = GameObject.Find("Enemy(Clone)");
            if (turretScene == null)
                PhotonNetwork.Instantiate(prefabTurret.name, referenceTurret.transform.position, Quaternion.identity, 0);
            else return;
        }
    }
}
