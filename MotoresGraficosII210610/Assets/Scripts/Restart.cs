﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class RestartEvent : System.EventArgs
{
    public int actualEnum { get; set; }
    public GameObject ball { get; set; }
    public GameObject Player { get; set; }
    public Torreta TowerGameObject { get; set; }
    public List<Controller_Bullet> HitsBullet { get; set; }
}
public class Restart
{
    public delegate void Reset(object sender, RestartEvent args);
    public event Reset eventoReset;
    public virtual void OnRestart(int AEnum, GameObject bullet, GameObject player, Torreta tower, List<Controller_Bullet> historicalBullets)
    {
        eventoReset?.Invoke(this, new RestartEvent() { actualEnum = AEnum, ball = bullet, Player = player, TowerGameObject = tower, HitsBullet = historicalBullets });
    }
}
