﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuBehaviour : MonoBehaviour
{
    public void OnPlayButton()
    {
        SceneManager.LoadScene(1, LoadSceneMode.Single);
    }
    public void OnExitButton()
    {
        Application.Quit();
        Debug.Log("Aquí se cerraría");
    }
}
