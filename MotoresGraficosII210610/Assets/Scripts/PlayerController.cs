﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using Photon.Realtime;
using ExitGames.Client.Photon;
public class PlayerController : Photon.MonoBehaviour, IJugador
{

    public PhotonView photonView;
   
    private Rigidbody rb;
    private bool enElSuelo = true;
    public float Impulso;

    public GameObject selfCamera;
    public Controller_PlayerColor pColor;
    public MasterScript master;
    public GameObject turret;
    public static Vector3 startPos;


    DAO DAO;
    //IMPLEMENTATION OF INTERFACE
    public float velocity { get; set; }
    public int cSwitchTimes { get; set; }
    public string lastColor { get; set; }
    public bool canChangeColor { get; set; }
    public int deaths { get; set; }
    public void resolveChangeColor(bool yesOrNo)
    {
        if (yesOrNo == canChangeColor)
        {
            pColor.pView.RPC("ModifyValue", PhotonTargets.Others);
            //  pColor.ModifyValue();
            lastColor = pColor.color.ToString();
            cSwitchTimes++;
            canChangeColor = false;
            MasterScript.sCPlayerValue += cSwitchTimes;
        }
    }

    private void Awake()
    {
        if (!photonView.isMine) selfCamera.GetComponent<Camera>().enabled = false;//para no ver la cámara de otros jugadores
        rb = GetComponent<Rigidbody>();
        GameObject masterScript = GameObject.Find("MasterScript");
        master = masterScript.GetComponent<MasterScript>();
        pColor = masterScript.GetComponent<Controller_PlayerColor>();
        master.OnChangedScene();
        startPos = this.transform.position;
        DAO = GameObject.Find("DAO").GetComponent<DAO>();
        InvokeRepeating("SendInfoAboutPosition", 4, 4);
    }

    private void SendInfoAboutPosition()
    {
        DAO.OnAddingValuesToSQL(transform.position.x.ToString(), "player_pos_x");
        DAO.OnAddingValuesToSQL(transform.position.y.ToString(), "player_pos_y");
        DAO.OnAddingValuesToSQL(transform.position.z.ToString(), "player_pos_z");
    }
    [PunRPC]
    public void AskToWarp()
    {
        if (Time.timeScale == 1)
        {
            transform.position = startPos;
            rb.velocity = Vector3.zero;
            pColor.pView.RPC("ModifyReset", PhotonTargets.Others);//el jugador avisa a torreta que cambió de color y esta ejecuta la variable de cambio de color
            // pColor.ModifyValue();
        }
    }

    void Update()
    {

        if (photonView.isMine)//solo se mueve un cliente, no todos
        {
            float movimientoAdelanteAtras = Input.GetAxis("Vertical") * velocity;
            float movimientoCostados = Input.GetAxis("Horizontal") * velocity;

            movimientoAdelanteAtras *= Time.deltaTime;
            movimientoCostados *= Time.deltaTime;

            transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);

            if (Input.GetKeyDown(KeyCode.R))
            {
                if (turret != null)
                    EjectReset();
            }
        }
    }
    private void EjectReset()
    {
        if (turret == null) GetEnemy();
        master.OnReset(pColor.color.GetHashCode(), GameObject.Find("Bullet(Clone)"), this.gameObject, turret.GetComponent<Torreta>(), turret.GetComponent<Torreta>().historicalBullets);
        cSwitchTimes = 0;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (!collision.gameObject.CompareTag("Bullet") || !collision.gameObject.CompareTag("Torreta"))
        {
            if (collision.gameObject.CompareTag("Floor") || (collision.gameObject.CompareTag("Save") && pColor.color != 0) || (collision.gameObject.CompareTag("Blue") && pColor.color != (Controller_PlayerColor.ColorPlayer)1) || (collision.gameObject.CompareTag("Purple") && pColor.color != (Controller_PlayerColor.ColorPlayer)2) || (collision.gameObject.CompareTag("Red") && pColor.color != (Controller_PlayerColor.ColorPlayer)3))
            {
                EjectReset();
                deaths++;
            }
            else
            {
                enElSuelo = true;
            }
        }
        if (collision.gameObject.CompareTag("Bullet"))
        {
            canChangeColor = true;
            resolveChangeColor(true);
            if (enElSuelo == true)
            {
                rb.velocity = new Vector3(0f, Impulso, 0f);
                enElSuelo = false;
            }
        }
        if (collision.gameObject.CompareTag("Torreta"))
        {
            Time.timeScale = 0.8f;
            master.sceneChanged = true;
        }
    }

    public void GetEnemy()
    {
        turret = GameObject.Find("Enemy(Clone)");
    }
}
