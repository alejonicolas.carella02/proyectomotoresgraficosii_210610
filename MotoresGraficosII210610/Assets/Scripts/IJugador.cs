﻿public interface IJugador
{ 
    float velocity { get; set; }
    int cSwitchTimes { get; set; }
    int deaths { get; set; }
    string lastColor { get; set; }
    bool canChangeColor { get; set; }
    void resolveChangeColor(bool yesOrNo);
}
