﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Controller_LevelExchanger : IComparable<Controller_LevelExchanger>
{
    //IComparable para comparar entre clases, se utiliza la misma para luego al sortear la lista funcione "hardcodeado"
    public int actualLevel { get; set; }
    public string sceneName { get; set; }
    
    //Sobrecarga de métodos, primero como string luego como int
    public Controller_LevelExchanger(string sceneString)
    {
        sceneName = sceneString;
    }
    public Controller_LevelExchanger(int value)
    {
        actualLevel = value;
    }
    //Implementación de IComparable
    public int CompareTo(Controller_LevelExchanger other)
    {
        return actualLevel.CompareTo(other.actualLevel);
    }
}

public class LevelComparer
{
   
    public class StringLevelComparer : IComparer<Controller_LevelExchanger>
    {
        //Implementación de Icomparer, dónde especifico cómo comparar en base a String, con el INT para hacer integers de la cantidad de clases.
        public int Compare(Controller_LevelExchanger x, Controller_LevelExchanger y)
        {
            return x.sceneName.CompareTo(y.sceneName);
        }
    }
}
