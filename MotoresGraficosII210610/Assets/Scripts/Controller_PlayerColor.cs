﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Controller_PlayerColor : MonoBehaviour
{
    //Enum, y List de materiales, para así interpretar a qué color cambiar, en base al susodicho enum y el amount.
    public PhotonView pView;

    public enum ColorPlayer { Orange, Blue, Purple, Red}

    public ColorPlayer color;

    public GameObject player;

    public List<Material> materialColor = new List<Material>();

    public static int amount = 0;

    public void NewScene()
    {
        color = 0;
        player.GetComponent<MeshRenderer>().material = materialColor[color.GetHashCode()];
    }

    [PunRPC] //así se declara un rpc
    public void ModifyValue()
    {
        amount += 1;
        if (SceneManager.GetActiveScene().buildIndex > 1)
        {
            if (amount > 3)
                amount = 0;
        }
        else if (amount > 1)
            amount = 0;
        color = (ColorPlayer)amount;
        player.GetComponent<MeshRenderer>().material = materialColor[color.GetHashCode()];
    }
    [PunRPC]
    public void ModifyReset()
    {
        amount = 0;
        color = (ColorPlayer)amount;
        player.GetComponent<MeshRenderer>().material = materialColor[color.GetHashCode()];
    }
}
