﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Controller_Connection_UI : MonoBehaviour
{
    public InputField username, startUserName;
    public GameObject visitor, creator, start;
    public DAO DAO;
    private void Start()
    {
        PhotonNetwork.ConnectUsingSettings("0.1");//definir la versión del juego (si se tienen dif no funciona)
        PhotonNetwork.automaticallySyncScene = true; //sincronizar las escenas con todos los jugadores de la sala
        visitor.SetActive(false);
        creator.SetActive(false);
    }
    private void OnGUI()
    {
        GUILayout.Label(PhotonNetwork.connectionStateDetailed.ToString()); //estado de la network
    }
    public void OnConnectedToMaster()
    {
        Debug.Log("a");
        PhotonNetwork.JoinLobby(TypedLobby.Default);//te conecta al lobby
    }
    public void SetName()
    {
        if (username.text.Length >= 4)
        {
            visitor.SetActive(true); creator.SetActive(true);
        }
        else
        {
            visitor.SetActive(false); creator.SetActive(false);
        }
    }
    public void SetUserName()
    {
        start.SetActive(false);
        PhotonNetwork.playerName = startUserName.text; //cada jugador tiene un username
    }
    public void CreateGame()
    {
        if (PhotonNetwork.connectionStateDetailed.ToString().Equals("JoinedLobby")) //si hay conexión con photon
        {
            PhotonNetwork.CreateRoom(username.text, new RoomOptions() { maxPlayers = 2 }, TypedLobby.Default);//se crea la sala con todas sus especificaciones
            DAO.FirstExecutableSQL(PhotonNetwork.playerName.ToString(), "player");
        }
        else Debug.Log("Wait");
    }
    public void JoinGame()
    {
        if (PhotonNetwork.connectionStateDetailed.ToString().Equals("JoinedLobby"))
        {
            RoomOptions rO = new RoomOptions();
            rO.maxPlayers = 2;
            PhotonNetwork.JoinOrCreateRoom(username.text, rO, TypedLobby.Default);//te podes unir a una sala o te crea una si no está creada
            DAO.FirstExecutableSQL(PhotonNetwork.playerName.ToString(), "turret");
        }
        else Debug.Log("Wait");
    }
    
    private void OnJoinedRoom()
    {
        PhotonNetwork.LoadLevel("Level");//acá se va a la escena (level) con el nombre de la variable deseada

    }
}
